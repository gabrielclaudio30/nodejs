// Installa il modulo mongoose usando npm
// npm install mongoose

// Importa il modulo mongoose
const mongoose = require('mongoose');

// Connetti a MongoDB (assicurati di avere il server MongoDB in esecuzione)
mongoose.connect('mongodb://localhost:27017/transactionsDB', { useNewUrlParser: true, useUnifiedTopology: true });

// Definisci lo schema per le transazioni
const transactionSchema = new mongoose.Schema({
    name: String,
    amount: Number,
    date: Date,
    type: String // income or expense
});

// Crea un modello basato sullo schema
const Transaction = mongoose.model('Transaction', transactionSchema);

// Aggiungi una nuova transazione al database
const newTransaction = new Transaction({
    name: 'Example Transaction',
    amount: 100,
    date: new Date(),
    type: 'income'
});

newTransaction.save()
    .then(() => {
        console.log('Transaction added successfully');
    })
    .catch(err => {
        console.log(err);
    });

// Recupera tutte le transazioni dal database
Transaction.find()
    .then(transactions => {
        console.log(transactions);
    })
    .catch(err => {
        console.log(err);
    });